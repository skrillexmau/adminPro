import { NgModule } from '@angular/core';

import { HeaderComponent } from './header/header.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { NotpagefoundComponent } from './notpagefound/notpagefound.component';


@NgModule({
    declarations: [
        HeaderComponent,
        SidenavComponent,
        BreadcrumbsComponent,
        NotpagefoundComponent
    ],
    exports: [
        HeaderComponent,
        SidenavComponent,
        BreadcrumbsComponent,
        NotpagefoundComponent
    ]
})

export class SharedModule {}