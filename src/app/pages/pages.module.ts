import { NgModule } from '@angular/core';

/* ng2-charts */
import { ChartsModule } from 'ng2-charts';

/* Routes */
import { PAGES_ROUTES } from './pages.routes';

/* Modules */
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms'

import { PagesComponent } from './pages.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { ProgressComponent } from './progress/progress.component';
import { Graficas1Component } from './graficas1/graficas1.component';

import { IncrementadorComponent } from '../components/incrementador/incrementador.component';    
import { GraficoDonasComponent } from '../components/grafico-donas/grafico-donas.component';



@NgModule({
    declarations: [
        PagesComponent,
        DashboardComponent,
        ProgressComponent,
        Graficas1Component,
        IncrementadorComponent,
        GraficoDonasComponent
    ],
    exports: [
        DashboardComponent,
        ProgressComponent,
        Graficas1Component
    ],
    imports: [
        SharedModule,
        PAGES_ROUTES,
        FormsModule,
        ChartsModule
    ]
})

export class PagesModule { }